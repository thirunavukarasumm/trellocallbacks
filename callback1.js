const fs = require("fs");
const path = require("path");

/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function boardInformation(boardID, callback) {
    let boardsFile = "./Data/boards.json"
    let resultFile = "callback1.json"
    setTimeout(() => {
        fs.readFile(path.join(__dirname, boardsFile), (err, data) => {
            if (err) {
                callback(err);
            } else {
                let resultData = JSON.parse(data).filter((villian) => {
                    return villian.id === boardID;
                });
                
                fs.writeFile(path.join(__dirname, resultFile), JSON.stringify(resultData),(err,data)=>{
                    if(err){
                        if (err.code === "EACCES") {
                            console.log("The file can't be created as the access is denied to the given location.");
                        } else if (err.code === "EEXIST") {
                            console.log("The file already exists.");
                        }
                    }else{
                        callback(null, `The output is written into ${resultFile}`);
                       
                    }
                })
                
            }
        })
    }, 2 * 1000);
}

module.exports = boardInformation;