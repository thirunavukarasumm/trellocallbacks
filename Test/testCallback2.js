const callback2 = require("../callback2");

callback2("mcu453ed", (err, data) => {
    if (err) {
        if (err.code === "ENOENT") {
            console.log(`File doesn't exist - ${err.path}`);
        }
    } else {
        console.log(data);
    }
});

