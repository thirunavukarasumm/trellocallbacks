const fs = require("fs");
const path = require("path");

/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/



function boardInformation(boardID, callback) {
    let listFile = "./Data/lists.json"
    let resultFile = "callback2.json"
    setTimeout(() => {
        fs.readFile(path.join(__dirname, listFile), (err, data) => {
            if (err) {
                callback(err);
            } else {
                let resultData;
                if (JSON.parse(data)[boardID] === undefined) {
                    resultData = {};
                } else {
                    resultData = JSON.parse(data)[boardID];
                }

                fs.writeFile(path.join(__dirname, resultFile), JSON.stringify(resultData), (err) => {
                    if (err) {
                        if (err.code === "EACCES") {
                            console.log("The file can't be created as the access is denied to the given location.");
                        } else if (err.code === "EEXIST") {
                            console.log("The file already exists.");
                        }
                    } else {
                        callback(null, `The output is written into ${resultFile}`);

                    }
                })

            }
        })
    }, 2 * 1000);
}

module.exports = boardInformation;